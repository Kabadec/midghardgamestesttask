using Core.Views;
using UnityEngine;

namespace Core.Components
{
    public struct Cell
    {
        public int SphereEntity;
        public Vector3 SphereTargetPosition;
        public CellView View;
    }
}