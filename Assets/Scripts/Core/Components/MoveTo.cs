using UnityEngine;

namespace Core.Components
{
    public struct MoveTo
    {
        public Vector3 TargetPosition;
    }
}