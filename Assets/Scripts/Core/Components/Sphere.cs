
using Core.Views;
using UnityEngine;

namespace Core.Components
{
    public struct Sphere
    {
        public Vector3 Position;
        public float Scale;
        public SphereView SphereView;
    }
}