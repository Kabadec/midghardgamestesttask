using Core.Shared;
using Core.Shared.Data;
using Core.Shared.Definitions;
using Core.Systems;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core
{
    public class CoreWorld : MonoBehaviour
    {
        [SerializeField] private CoreData m_CoreData;
        [SerializeField] private CoreDefinition m_CoreDefinition;

        private EcsSystems m_Systems;
        private EcsWorld m_World;

        public void Init()
        {
            var sharedData = new SharedData(m_CoreData, m_CoreDefinition);
            m_World = new EcsWorld();
            m_Systems = new EcsSystems(m_World, sharedData);

            m_Systems
                .Add(new GridInitSystem())
                .Add(new MouseInputSystem())
                .Add(new SphereSpawnerSystem())
                .Add(new SphereCursorFollowersSystem())
                .Add(new PlacingSpheresSystem())
                .Add(new SphereScaleUpSystem())
                .Add(new SphereMoveToSystem())
#if UNITY_EDITOR
                .Add (new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem ())
#endif
                .Init();
        }

        private void Update()
        {
            m_Systems.Run();
        }

        private void OnDestroy()
        {
            if (m_Systems != null)
            {
                m_Systems.Destroy();
                m_Systems = null;
            }

            if (m_World != null)
            {
                m_World.Destroy();
                m_World = null;
            }
        }
    }
}