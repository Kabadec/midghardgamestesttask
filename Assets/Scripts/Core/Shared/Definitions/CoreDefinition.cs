using Core.Views;
using UnityEngine;

namespace Core.Shared.Definitions
{
    [CreateAssetMenu(fileName = nameof(CoreDefinition), menuName = "Core/" + nameof(CoreDefinition))]
    public class CoreDefinition : ScriptableObject
    {
        [SerializeField] private SphereView m_SphereViewPrefab;
        
        [Header("Follow Settings")]
        [SerializeField, Range(0f, 1f)] private float m_CursorFollowerLerpValue;

        [Header("Scale Settings")]
        [SerializeField] private float m_SphereStartScale;
        [SerializeField] private float m_SphereDefaultScale;
        [SerializeField, Range(0f, 1f)] private float m_SphereScaleLerpValue;
        
        [Header("Move Settings")]
        [SerializeField, Range(0f, 1f)] private float m_SphereMoveLerpValue;

        public SphereView SphereViewPrefab => m_SphereViewPrefab;
        public float CursorFollowerLerpValue => m_CursorFollowerLerpValue;
        public float SphereStartScale => m_SphereStartScale;
        public float SphereDefaultScale => m_SphereDefaultScale;
        public float SphereScaleLerpValue => m_SphereScaleLerpValue;
        public float SphereMoveLerpValue => m_SphereMoveLerpValue;
    }
}