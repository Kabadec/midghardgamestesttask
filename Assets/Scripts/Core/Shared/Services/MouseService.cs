using UnityEngine;

namespace Core.Shared.Services
{
    public class MouseService
    {
        public Vector2 Position { get; private set; }
        public bool LeftMouseButtonDown { get; private set; }

        public void SetPosition(Vector2 position)
        {
            Position = position;
        }

        public void SetLeftMouseButtonDown(bool isDown)
        {
            LeftMouseButtonDown = isDown;
        }
    }
}