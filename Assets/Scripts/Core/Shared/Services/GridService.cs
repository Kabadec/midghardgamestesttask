using UnityEngine;

namespace Core.Shared.Services
{
    public class GridService
    {
        private int[] m_Cells;
        private int m_Width;
        private int m_Height;
        private Vector3 m_GridStartPosition;
        private float m_CellSize;

        public void Init(int width, int height, Vector3 worldStartPosition, float cellSize)
        {
            m_CellSize = cellSize;
            m_GridStartPosition = worldStartPosition;
            m_Cells = new int[width * height];
            m_Width = width; 
            m_Height = height;
        }

        public Vector2Int ConvertWorldPosToCellCoords(Vector3 clickPosition)
        {
            var localClickPosition = clickPosition - m_GridStartPosition;
            var floatX = localClickPosition.x / m_CellSize;
            var floatY = localClickPosition.z / m_CellSize;
            var x = floatX > 0 ? (int)floatX : (int)floatX - 1;
            var y = floatY > 0 ? (int)floatY : (int)floatY - 1;
            return new Vector2Int(x, y);
        }

        public bool AddCell(int cellEntity, Vector2Int coords)
        {
            if (!InBounds(coords))
            {
                return false;
            }

            var index = m_Width * coords.y + coords.x;
            if (m_Cells[index] > 0)
            {
                return false;
            }
            
            m_Cells[index] = cellEntity + 1;
            return true;
        }

        public int GetCellEntity(Vector2Int coords)
        {
            if (!InBounds(coords))
            {
                return -1;
            }
            
            return m_Cells[m_Width * coords.y + coords.x] - 1;
        }
        
        private bool InBounds(Vector2Int coords)
        {
            return coords.x >= 0 &&
                   coords.x < m_Width
                   && coords.y >= 0
                   && coords.y < m_Height;
        }
    }
}