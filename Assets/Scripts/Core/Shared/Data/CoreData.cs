using UnityEngine;

namespace Core.Shared.Data
{
    public class CoreData : MonoBehaviour
    {
        [SerializeField] private GridData m_GridData;
        [Space]
        [SerializeField] private Transform m_SphereViewContainer;

        [Header("Input planes normals")]
        [SerializeField] private Transform m_InputNormal;
        [SerializeField] private Transform m_CursorFollowerNormal;

        public GridData GridData => m_GridData;
        public Transform SphereViewContainer => m_SphereViewContainer;
        public Transform InputNormal => m_InputNormal;
        public Transform CursorFollowerNormal => m_CursorFollowerNormal;
    }
}