using System.Collections.Generic;
using Core.Views;
using UnityEditor;
using UnityEngine;

namespace Core.Shared.Data
{
    public class GridData : MonoBehaviour
    {
        [SerializeField] private int m_Width;
        [SerializeField] private int m_Height;
        [SerializeField] private float m_CellSize;
        [SerializeField] private Transform m_GridStartPosition;
        [SerializeField] private List<CellView> m_CellViews;
#if UNITY_EDITOR
        [SerializeField] private CellView m_CellViewPrefab;
#endif

        public int Width => m_Width;
        public int Height => m_Height;
        public float CellSize => m_CellSize;
        public Transform GridStartPosition => m_GridStartPosition;
        public List<CellView> CellViews => m_CellViews;

#if UNITY_EDITOR

        [ContextMenu("Rebuild Cells")]
        private void RebuildCells()
        {
            for (var i = m_GridStartPosition.childCount - 1; i >= 0; i--)
            {
                DestroyImmediate(m_GridStartPosition.GetChild(i).gameObject);
            }

            m_CellViews = new List<CellView>();
            var startPos = new Vector2(m_CellSize / 2f, m_CellSize / 2f);
            for (var i = 0; i < m_Width * m_Height; i++)
            {
                var x = i % m_Width;
                var y = i / m_Width;
                var targetPosition = new Vector3(x * m_CellSize + startPos.x, 0f, y * m_CellSize + startPos.y);
                var cellView = PrefabUtility.InstantiatePrefab(m_CellViewPrefab, m_GridStartPosition) as CellView;
                cellView.gameObject.name = $"CellView {i}: ({x}, {y})";
                cellView.transform.localPosition = targetPosition;
                m_CellViews.Add(cellView);
            }

            EditorUtility.SetDirty(this);
        }
#endif
    }
}