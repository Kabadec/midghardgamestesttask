using Core.Shared.Data;
using Core.Shared.Definitions;
using Core.Shared.Services;

namespace Core.Shared
{
    public class SharedData
    {
        public CoreData CoreData { get; private set; }
        public CoreDefinition CoreDefinition { get; private set; }
        public MouseService MouseService { get; private set; }
        public GridService GridService { get; private set; }

        public SharedData(CoreData coreData, CoreDefinition coreDefinition)
        {
            CoreData = coreData;
            CoreDefinition = coreDefinition;
            MouseService = new MouseService();
            GridService = new GridService();
        }
    }
}