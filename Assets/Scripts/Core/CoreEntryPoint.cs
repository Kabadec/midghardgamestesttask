using UnityEngine;

namespace Core
{
    public class CoreEntryPoint : MonoBehaviour
    {
        [SerializeField] private CoreWorld m_CoreWorld;
        
        private void Start()
        {
            m_CoreWorld.Init();
        }
    }
}