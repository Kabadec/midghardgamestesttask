using Core.Components;
using Core.Shared;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core.Systems
{
    public class GridInitSystem : IEcsInitSystem
    {
        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var cellPool = world.GetPool<Cell>();
            var sharedData = systems.GetShared<SharedData>();
            var gridService = sharedData.GridService;
            var gridData = sharedData.CoreData.GridData;
            gridService.Init(
                gridData.Width,
                gridData.Height,
                gridData.GridStartPosition.position,
                gridData.CellSize);

            for (var i = 0; i < gridData.CellViews.Count; i++)
            {
                var cellView = gridData.CellViews[i];
                
                var entity = world.NewEntity();
                ref var cell = ref cellPool.Add(entity);
                
                cell.SphereEntity = -1;
                cell.SphereTargetPosition = cellView.SpherePosition.position;
                cell.View = cellView;

                var x = i % gridData.Width;
                var y = i / gridData.Width;
                gridService.AddCell(entity, new Vector2Int(x, y));
            }
        }
    }
}