using Core.Components;
using Core.Shared;
using Core.Shared.Services;
using Core.Utilities;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core.Systems
{
    public class SphereCursorFollowersSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter m_SphereCursorFollowersFilter;
        private EcsPool<Sphere> m_SpherePool;
        
        private MouseService m_MouseService;
        private MouseToWorldPositionConverter m_MousePositionConverter;
        private float m_CursorFollowerLerpValue;

        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            m_SphereCursorFollowersFilter = world
                .Filter<Sphere>()
                .Inc<CursorFollower>()
                .End();
            m_SpherePool = world.GetPool<Sphere>();
            
            var sharedData = systems.GetShared<SharedData>();
            m_CursorFollowerLerpValue = sharedData.CoreDefinition.CursorFollowerLerpValue;
            m_MouseService = sharedData.MouseService;

            var normal = sharedData.CoreData.CursorFollowerNormal;
            m_MousePositionConverter = new MouseToWorldPositionConverter(Camera.main, normal);
        }

        public void Run(IEcsSystems systems)
        {
            foreach (var entity in m_SphereCursorFollowersFilter)
            {
                ref var sphere = ref m_SpherePool.Get(entity);
                sphere.Position = Vector3.Lerp(
                    sphere.SphereView.Transform.position,
                    m_MousePositionConverter.GetMouseToWorldPosition(m_MouseService.Position),
                    m_CursorFollowerLerpValue);

                sphere.SphereView.Transform.position = sphere.Position;
            }
        }
    }
}