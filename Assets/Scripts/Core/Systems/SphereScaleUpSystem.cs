using Core.Components;
using Core.Shared;
using Core.Views;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core.Systems
{
    public class SphereScaleUpSystem : IEcsInitSystem, IEcsRunSystem
    {
        private const float m_EndDistance = 0.05f;
        
        private EcsFilter m_ScalableSpheresFilter;
        private EcsPool<Sphere> m_SpherePool;
        private EcsPool<ScaleUp> m_ScaleUpPool;

        private float m_ScaleLerpValue;

        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            m_ScalableSpheresFilter = world
                .Filter<Sphere>()
                .Inc<ScaleUp>()
                .End();
            m_SpherePool = world.GetPool<Sphere>();
            m_ScaleUpPool = world.GetPool<ScaleUp>();

            var sharedData = systems.GetShared<SharedData>();
            m_ScaleLerpValue = sharedData.CoreDefinition.SphereScaleLerpValue;
        }

        public void Run(IEcsSystems systems)
        {
            foreach (var sphereEntity in m_ScalableSpheresFilter)
            {
                ref var sphere = ref m_SpherePool.Get(sphereEntity);
                ref var scaleUp = ref m_ScaleUpPool.Get(sphereEntity);
                
                if (Mathf.Abs(scaleUp.TargetScale - sphere.Scale) <= m_EndDistance)
                {
                    sphere.Scale = scaleUp.TargetScale;
                    UpdateSphereViewScale(sphere.SphereView, sphere.Scale);
                    m_ScaleUpPool.Del(sphereEntity);
                    continue;
                }

                sphere.Scale = Mathf.Lerp(sphere.Scale, scaleUp.TargetScale, m_ScaleLerpValue);
                UpdateSphereViewScale(sphere.SphereView, sphere.Scale);
            }
        }

        private void UpdateSphereViewScale(SphereView view, float scale)
        {
            view.Transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}