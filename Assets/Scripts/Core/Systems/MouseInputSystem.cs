using Core.Shared;
using Core.Shared.Services;
using Leopotam.EcsLite;
using Input = UnityEngine.Input;

namespace Core.Systems
{
    public class MouseInputSystem : IEcsInitSystem, IEcsRunSystem
    {
        private MouseService m_MouseService;

        public void Init(IEcsSystems systems)
        {
            m_MouseService = systems.GetShared<SharedData>().MouseService;
        }

        public void Run(IEcsSystems systems)
        {
            m_MouseService.SetPosition(Input.mousePosition);
            m_MouseService.SetLeftMouseButtonDown(Input.GetMouseButtonDown(0));
        }
    }
}