using Core.Components;
using Core.Shared;
using Core.Shared.Services;
using Core.Utilities;
using Core.Views;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core.Systems
{
    public class SphereSpawnerSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter m_FollowSpheresFilter;

        private EcsPool<Sphere> m_SpherePool;
        private EcsPool<CursorFollower> m_CursorFollowersPool;
        private EcsPool<ScaleUp> m_ScaleUpPool;

        private SphereView m_SphereViewPrefab;
        private Transform m_SphereViewParent;
        private MouseService m_MouseService;
        private MouseToWorldPositionConverter m_PositionConverter;

        private float m_StartScale;
        private float m_TargetScale;

        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            m_FollowSpheresFilter = world
                .Filter<Sphere>()
                .Inc<CursorFollower>()
                .End();
            m_SpherePool = world.GetPool<Sphere>();
            m_CursorFollowersPool = world.GetPool<CursorFollower>();
            m_ScaleUpPool = world.GetPool<ScaleUp>();

            var sharedData = systems.GetShared<SharedData>();
            m_MouseService = sharedData.MouseService;
            m_SphereViewPrefab = sharedData.CoreDefinition.SphereViewPrefab;
            m_StartScale = sharedData.CoreDefinition.SphereStartScale;
            m_TargetScale = sharedData.CoreDefinition.SphereDefaultScale;
            m_SphereViewParent = sharedData.CoreData.SphereViewContainer;
            
            var normal = sharedData.CoreData.CursorFollowerNormal;
            m_PositionConverter = new MouseToWorldPositionConverter(Camera.main, normal);
        }

        public void Run(IEcsSystems systems)
        {
            if (m_FollowSpheresFilter.GetEntitiesCount() > 0)
            {
                return;
            }

            var world = systems.GetWorld();
            SpawnSphere(world);
        }

        private void SpawnSphere(EcsWorld world)
        {
            var entity = world.NewEntity();
            ref var sphere = ref m_SpherePool.Add(entity);
            m_CursorFollowersPool.Add(entity);
            ref var scaleUp = ref m_ScaleUpPool.Add(entity);
            scaleUp.TargetScale = m_TargetScale;
            
            var spawnPosition = m_PositionConverter.GetMouseToWorldPosition(m_MouseService.Position);
            var view = Object.Instantiate(m_SphereViewPrefab, spawnPosition, Quaternion.identity, m_SphereViewParent);
            sphere.Position = spawnPosition;
            sphere.Scale = m_StartScale;
            sphere.SphereView = view;
            view.Transform.localScale = new Vector3(m_StartScale, m_StartScale, m_StartScale);
        }
    }
}