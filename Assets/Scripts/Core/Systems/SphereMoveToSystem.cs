using Core.Components;
using Core.Shared;
using Core.Views;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core.Systems
{
    public class SphereMoveToSystem : IEcsInitSystem, IEcsRunSystem
    {
        private const float m_StopDistance = 0.001f;

        private EcsFilter m_MovableSpheresFilter;
        private EcsPool<Sphere> m_SpherePool;
        private EcsPool<MoveTo> m_MoveToPool;

        private float m_MoveLerpValue;

        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            m_MovableSpheresFilter = world
                .Filter<Sphere>()
                .Inc<MoveTo>()
                .End();
            m_SpherePool = world.GetPool<Sphere>();
            m_MoveToPool = world.GetPool<MoveTo>();

            var sharedData = systems.GetShared<SharedData>();
            m_MoveLerpValue = sharedData.CoreDefinition.SphereMoveLerpValue;
        }

        public void Run(IEcsSystems systems)
        {
            foreach (var sphereEntity in m_MovableSpheresFilter)
            {
                ref var sphere = ref m_SpherePool.Get(sphereEntity);
                ref var moveTo = ref m_MoveToPool.Get(sphereEntity);


                if ((sphere.Position - moveTo.TargetPosition).sqrMagnitude <= m_StopDistance)
                {
                    sphere.Position = Vector3.Lerp(sphere.Position, moveTo.TargetPosition, m_MoveLerpValue);
                    UpdateSphereViewPosition(sphere.SphereView, sphere.Position);
                    m_MoveToPool.Del(sphereEntity);
                    continue;
                }

                sphere.Position = Vector3.Lerp(sphere.Position, moveTo.TargetPosition, m_MoveLerpValue);
                UpdateSphereViewPosition(sphere.SphereView, sphere.Position);
            }
        }

        private void UpdateSphereViewPosition(SphereView view, Vector3 position)
        {
            view.Transform.position = position;
        }
    }
}