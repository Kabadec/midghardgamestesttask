using Core.Components;
using Core.Shared;
using Core.Shared.Services;
using Core.Utilities;
using Leopotam.EcsLite;
using UnityEngine;

namespace Core.Systems
{
    public class PlacingSpheresSystem : IEcsInitSystem, IEcsRunSystem
    {
        private EcsFilter m_PlaceableSpheresFilter;
        private EcsPool<Cell> m_CellPool;
        private EcsPool<MoveTo> m_MoveToPool;
        private EcsPool<CursorFollower> m_CursorFollowerPool;

        private MouseService m_MouseService;
        private GridService m_GridService;
        private MouseToWorldPositionConverter m_MousePositionConverter;

        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            m_PlaceableSpheresFilter = world
                .Filter<Sphere>()
                .Inc<CursorFollower>()
                .Exc<ScaleUp>()
                .End();
            m_CellPool = world.GetPool<Cell>();
            m_MoveToPool = world.GetPool<MoveTo>();
            m_CursorFollowerPool = world.GetPool<CursorFollower>();

            var sharedData = systems.GetShared<SharedData>();
            m_MouseService = sharedData.MouseService;
            m_GridService = sharedData.GridService;

            var inputNormal = sharedData.CoreData.InputNormal;
            m_MousePositionConverter = new MouseToWorldPositionConverter(Camera.main, inputNormal);
        }

        public void Run(IEcsSystems systems)
        {
            if (!m_MouseService.LeftMouseButtonDown || m_PlaceableSpheresFilter.GetEntitiesCount() <= 0)
            {
                return;
            }

            var cellEntity = GetCellEntity();
            if (cellEntity == -1)
            {
                return;
            }

            ref var cell = ref m_CellPool.Get(cellEntity);
            if (cell.SphereEntity != -1)
            {
                return;
            }

            var targetSphereEntity = GetTargetSphereEntity();
            if (targetSphereEntity == -1)
            {
                return;
            }

            cell.SphereEntity = targetSphereEntity;
            m_CursorFollowerPool.Del(targetSphereEntity);
            ref var moveTo = ref m_MoveToPool.Add(targetSphereEntity);
            moveTo.TargetPosition = cell.SphereTargetPosition;
        }

        private int GetTargetSphereEntity()
        {
            foreach (var sphereEntity in m_PlaceableSpheresFilter)
            {
                return sphereEntity;
            }

            return -1;
        }

        private int GetCellEntity()
        {
            var clickPosition = m_MousePositionConverter.GetMouseToWorldPosition(m_MouseService.Position);
            var coords = m_GridService.ConvertWorldPosToCellCoords(clickPosition);
            var cellEntity = m_GridService.GetCellEntity(coords);
            return cellEntity;
        }
    }
}