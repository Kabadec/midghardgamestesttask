using UnityEngine;

namespace Core.Utilities
{
    public class MouseToWorldPositionConverter
    {
        private readonly Camera m_Camera;
        private Plane m_Plane;
        private Ray m_Ray;

        public MouseToWorldPositionConverter(Camera camera, Transform normal)
        {
            m_Camera = camera;
            m_Plane = new Plane(normal.up, normal.position);
        }

        public Vector3 GetMouseToWorldPosition(Vector2 mousePosition)
        {
            m_Ray = m_Camera.ScreenPointToRay(mousePosition);
            m_Plane.Raycast(m_Ray, out var distance);
            return m_Ray.GetPoint(distance);
        }
    }
}