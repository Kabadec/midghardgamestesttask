using UnityEngine;

namespace Core.Views
{
    public class CellView : MonoBehaviour
    {
        [SerializeField] private Transform m_SpherePosition;

        public Transform SpherePosition => m_SpherePosition;
    }
}