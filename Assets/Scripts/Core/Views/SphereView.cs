using UnityEngine;

namespace Core.Views
{
    [SelectionBase]
    public class SphereView : MonoBehaviour
    {
        [SerializeField] private Transform m_Transform;

        public Transform Transform => m_Transform;
    }
}