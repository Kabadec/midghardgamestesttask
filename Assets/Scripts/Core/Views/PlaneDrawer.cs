using UnityEngine;

namespace Core.Views
{
    public class PlaneDrawer : MonoBehaviour
    {
#if UNITY_EDITOR
        private void OnDrawGizmos(){
            Color32 color = Color.blue;
            color.a = 125;
            var rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
            
            Gizmos.color = color;
            Gizmos.matrix = rotationMatrix;

            Gizmos.DrawCube(Vector3.zero, new Vector3(1f, 0.0001f, 1f));
            
            Gizmos.matrix = Matrix4x4.identity;
            Gizmos.color = Color.white;
        }
#endif
    }
}