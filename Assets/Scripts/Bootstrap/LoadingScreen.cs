using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace Bootstrap
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private CanvasGroup m_CanvasGroup;
        [SerializeField] private RectTransform m_Icon;

        private Tween m_RotationTween;
        private Tween m_FadeTween;
        
        public void Enable()
        {
            KillTweens();
            m_CanvasGroup.alpha = 1f;
            gameObject.SetActive(true);
            m_RotationTween = CreateRotationTween();
        }

        public void Disable()
        {
            m_FadeTween = CreateFadeTween();
        }

        private TweenerCore<float, float, FloatOptions> CreateRotationTween()
        {
            return DOTween.To(
                    () => m_Icon.localRotation.eulerAngles.z,
                    z => m_Icon.localRotation = Quaternion.Euler(new Vector3(0f, 0f, z)),
                    -360f,
                    3f)
                .SetEase(Ease.Linear)
                .SetLoops(-1);
        }

        private TweenerCore<float, float, FloatOptions> CreateFadeTween()
        {
            return m_CanvasGroup
                .DOFade(0f, 0.5f)
                .OnComplete(
                    () =>
                    {
                        m_RotationTween?.Kill();
                        gameObject.SetActive(false);
                    });
        }

        private void KillTweens()
        {
            m_FadeTween?.Kill();
            m_RotationTween?.Kill();
        }
    }
}