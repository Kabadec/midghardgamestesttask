using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Bootstrap
{
    public class BootstrapEntryPoint : MonoBehaviour
    {
        private const float m_ResourcesLoadingDuration = 5f;
        private const string m_CoreSceneName = "CoreScene";

        [SerializeField] private LoadingScreen m_LoadingScreen;
        
        private async UniTaskVoid Start()
        {
            DontDestroyOnLoad(m_LoadingScreen.gameObject);
            m_LoadingScreen.Enable();
            await Init();
            m_LoadingScreen.Disable();
        }

        private async UniTask Init()
        {
            await LoadResources();
            await SceneManager.LoadSceneAsync(m_CoreSceneName);
        }

        private async UniTask LoadResources()
        {
            await UniTask.WaitForSeconds(m_ResourcesLoadingDuration);
        }
    }
}